#!/usr/bin/python
# -*- coding: utf-8 -*-

# Historic Graphs was created by Antonin Segault in 2019
# Info and updates : https://framagit.org/retrodev/historic-graphs

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import requests, time


# configuration variables

pages = ['Dorayaki'] # articles used as starting point

dates = ['2006-01-01T00:00:00Z', '2012-01-01T00:00:00Z', '2018-01-01T00:00:00Z'] # revision dates (ISO)
# careful, UTC+0 timezone will be used, even when using -- for exemple -- french wikipedia

lang = 'en' # language version of wikipedia that is queried

depth = 1 # increasing leads to larger graph, but very long data collection times

trim = True # whether or not we should remove the external leafs of the graph (usually cleaner)


# stop list : title of sections that may indicate the end of the content of a page
# might need to be expanded -- or even created if you want to use other languages

stop = {}
stop['fr'] = ['Annexes', 'Articles connexes', 'Bibliographie', 'Notes', 'Notes et références', 'Références', 'Sources', 'Voir aussi']
stop['en'] = ['Notes', 'References', 'See also', 'Sources']


# utility functions

def handleRequest(url) :
	try:
		r = requests.get(url,timeout=30)
		r.raise_for_status()
	except requests.exceptions.HTTPError as errh:
		print ("Http Error:",errh)
	except requests.exceptions.ConnectionError as errc:
		print ("Error Connecting:",errc)
	except requests.exceptions.Timeout as errt:
		print ("Timeout Error:",errt)
	except requests.exceptions.RequestException as err:
		print ("OOps: Something Else",err)
	return r.json()


def getLinks(revid, sections) :
	links = []
	for section in sections :
		url = 'https://' + lang + '.wikipedia.org/w/api.php?action=parse&oldid=' + revid + '&prop=links&section=' + section + '&format=json'
		d = handleRequest(url)
		time.sleep(.25) # waiting after each request, because we don't want to flood the API
		if 'parse' in d :
			for link in d['parse']['links'] :
				if link['ns'] == 0 and 'exists' in link :
					if link['*'] not in links :
						links.append(link['*'])
	return links


def getSections(revid) :
	url = 'https://' + lang + '.wikipedia.org/w/api.php?action=parse&oldid=' + revid + '&prop=sections&format=json'
	d = handleRequest(url)
	sections = ['0']
	if 'parse' in d :
		for section in d['parse']['sections'] :
			if section['toclevel'] == 1 :
				if lang in stop : # if we have a stop list
					if section['line'] in stop[lang] :
						#print( section['line'] ) # usefull when filling the stop lists
						return sections # breaking out of the loop
				sections.append(section['index'])
	return sections


def getRevisions(page, dates) :
	revids = {}
	for date in dates :
		url = 'https://' + lang + '.wikipedia.org/w/api.php?action=query&prop=revisions&titles=' + page + '&rvlimit=1&rvprop=timestamp|ids&rvdir=older&rvstart=' + date + '&format=json'
		d = handleRequest(url)
		pageid = list(d['query']['pages'].keys())[0]
		if 'revisions' in d['query']['pages'][pageid] :
			revid = str(d['query']['pages'][pageid]['revisions'][0]['revid'])
			revids[date] = revid
	return revids


def buildGraph(graph, page, dates, depth) :
	print(page, depth, time.strftime("%H:%M:%S", time.localtime()))
	graph[page] = {}
	graph[page]['revisions'] = {}
	revs = getRevisions(page, dates)
	for rev in revs :
		s = getSections(revs[rev])
		l = getLinks(revs[rev], s)
		graph[page]['revisions'][rev] = []
		for link in l :
			graph[page]['revisions'][rev].append(link)
			if link not in graph.keys() and depth > 0 :
				buildGraph(graph, link, dates, depth - 1)


def trimGraph(graph) :
	for page in graph :
		for rev in graph[page]['revisions'] :
			trimmed = graph[page]['revisions'][rev].copy()
			for link in graph[page]['revisions'][rev] :
				if link not in graph.keys() :
					trimmed.remove(link)
			graph[page]['revisions'][rev] = trimmed
				

# serious business starts here

t = time.strftime("%Y-%m-%d_%H-%M-%S",time.localtime())

graph = {}

for page in pages :
	buildGraph(graph, page, dates, depth)

if trim :
	trimGraph(graph)


# saving the graph of each revision date as a CSV table
for date in dates :
	o = 'source\ttarget\ttimeset\n'
	for page in graph :
		if date in graph[page]['revisions'] :
			for link in graph[page]['revisions'][date] :
				o = o + page + '\t' + link + '\t' + date + '\n'
	f = open(t + '_rev-' + date + '.csv', 'w')
	f.write(o)
	f.close()

# saving the configuration variables in a separated file
f = open(t + '_settings.txt', 'w')
f.write('Pages : ' + str(pages) + '\n\n')
f.write('Dates : ' + str(dates) + '\n\n')
f.write('Lang : ' + str(lang) + '\n\n')
if lang in stop :
	f.write('Stopwords : ' + str(stop[lang]) + '\n\n')
else :
	f.write('No stopword found for this language\n\n')
f.write('Depth : ' + str(depth) + '\n\n')
f.write('Trim : ' + str(trim))
f.close()


