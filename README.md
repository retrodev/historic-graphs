# Historic Graphs

Building dynamic maps of the hyperlinks between Wikipedia articles

## About

Historic Graphs is a tool (and a method) to capture, represent and study the evolution of the links between Wikipedia articles over time. Each article has its own version history, storing all the changes, additions and removal that occurred over the time. Starting from an article, a python script connects to Wikipedia's APIs and collects a list of the links it contained at different points in time. It will then follow these links and continue collecting up to the desired depth. All the lists of links are then merged them into nice tables, showing the link structure for each date. These tables can then be imported into network analysis tools such as Gephi, to be visualized as dynamic maps.


## How To

The script requires Python 3. It has only been tested on a GNU/Linux (Debian) system, but should work properly on others. It relies on the requests library, that can be install through pip.

Open the historic-graphs.py with your favorite text editor and change the configuration variables according to what you want to collect (starting article, dates, language, depth ...). Then open a terminal and run the python script. After a while, it will output one CSV file per date containing the list of links that existed at that time.


## Examples

[Tutorial](examples/tutorial) : A quick demo showing how to capture the link structure around an article at different points in time, and how to visualize it as a dynamic map using Gephi.

[H2PTM 2019 Conference](examples/h2ptm) : A case study on the early development of the Wikipedia article related to November 13th 2015 terrorist attacks in Paris.

[H2PTM 2021 Conference](https://framagit.org/bouchereaua/ia-wikipedia-h2ptm21) : A study on the representation of Artificial Intelligence in the french encyclopedia over 19 years, developed with [Aymeric Bouchereau](https://bouchereaua.net)


## Credit

Historic Graphs was created and is maintained by Antonin Segault (Paris Nanterre University, Dicen-IdF lab, France). For questions, suggestions and bug fixes : antonin.segault-AT-parisnanterre.fr

Historic Graphs is a free software. The python script, the documentation and the examples are released under the terms of the GNU General Public License version 3 : https://www.gnu.org/licenses/

