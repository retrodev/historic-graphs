# H2PTM Conference

A case study on the early development of the Wikipedia article related to November 13th 2015 terrorist attacks in Paris.

This work was produced for the 15th H2PTM Conference (Hypertextes et Hypermédias. Produits, Outils et Méthodes) held in Montbéliard (France) in 2019. It will be published by ISTE Editions in the conference's proceedings :

Segault, A. (2019). Historic Graphs : Cartographie dynamique des liens sur Wikipédia. _H2PTM 2019, De l’hypertexte aux humanités numériques_. Paris, France: ISTE Editions.


## First case

These two first maps represent the link structure around the article "Attentats du 13 novembre 2015 en France" (depth = 1) on November 13th 2015 at 23:00 UTC (among the ealry versions of the article, while the Bataclan attack was still ongoing) and five hours later, on November 14th 2015 at 4:00 UTC. We can see, on the left side, the quick development of the article, growing from 25 to more than 100 links, while the surrounding network remains mostly unchanged.

![H2PTM first case](img/h2ptm_case1.png)

Data sources (CSV and Gephi files) and high-res maps can be found here : [case1/](case1/)


## Second case

The next maps still represent the November 14th 2015 at 4:00 UTC timeframes. But six highly central nodes (refering to Paris, 2015, November 2015 ...) were manually deleted. Without these bridging nodes, the spatialisation can more clearly reveal clusters. On the right side, Gephi's community detection algorithm was also used to color different components of the map.

![H2PTM second case study](img/h2ptm_case2.png)

Data sources (CSV and Gephi files) and high-res maps can be found here : [case2/](case2/)


## Third case

This serie of maps shows the development of the article over the hours, from November 13th 2015 at 22:00 UTC to November 14th 2015 at 3:00 UTC, using a different spatialisation algorithm.

![H2PTM third case study](img/h2ptm_case3.png)

A GIF animation presenting the differences between each map was also produced, showing that the development of the article was almost exclusively incremental, with very few links deleted between each version.

![H2PTM third case study animated](img/h2ptm_case3.gif)

Data sources (CSV and Gephi files) and high-res maps can be found here : [case3/](case3/)


## Fourth case

The last map is based on two timeframes : November 13th 2015 at 21:00 UTC (before the creation of the article) and November 14th 2015 at 12:00 UTC. The starting node (Attentats du 13 novembre 2015 en France) was then deleted from the graph. This map therefore only shows (in black lines) the links that were added around this article between the two timeframes (that I called peripheral links).

![H2PTM fourth case study](img/h2ptm_case4.png)

Data sources (CSV and Gephi files) and high-res maps can be found here : [case4/](case4/)


## Credit

Historic Graphs was created and is maintained by Antonin Segault (Paris Nanterre University, Dicen-IdF lab, France). For questions, suggestions and bug fixes : antonin.segault-AT-parisnanterre.fr

Historic Graphs is a free software. The python script, the documentation and the examples are released under the terms of the GNU General Public License version 3 : https://www.gnu.org/licenses/

