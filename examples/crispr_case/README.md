# A case on CRISPR-Cas9

Two months ago, Omer Benjakob and Rona Aviram, whom I met a few years ago, sent me this article in which they retraced the evolution of a set of Wikipedia articles retated to CRISPR-Cas9 genome editing tools :

Benjakob, O., Guley, O., Sevin, J. M., Blondel, L., Augustoni, A., Collet, M., ... & Aviram, R. (2023). Wikipedia as a tool for contemporary history of science: A case study on CRISPR. *Plos one, 18*(9), https://journals.plos.org/plosone/article/file?id=10.1371/journal.pone.0290827

This study resonates quite well with the [work Aymeric Bouchereau and myself have been doing on the topic of IA](https://framagit.org/retrodev/wp_ia_synthese_raisonnee). Discussing with Rona Aviram about the similarities in our approches, I decided to have a look to the link graph of their corpus.

## Using Historic Graphs to collect data

I firstly used the Python script to dig into Wikipedia's version history. I used the 51 Wikipedia articles identified by Benjakob et al. as starting points. In order to only map the links between these articles, I set the script's depth to zero, thereby preventing it to follow other links. I choose a temporal resolution of two years, building the link graph at the beginning of the years 2004, 2006, 2008, 2010, 2012, 2014, 2016, 2018, 2020 and 2022. I usually advice not to use so many timesteps but, with such a small corpus (due to the depth of 0), the data collection only took 18 minutes.

The data collected is provided in the following CSV files (the fiest date is the data collection one, the second gives you the timestep) :

- 2023-11-23\_20-25-01\_rev-2004-01-01T00:00:00Z.csv
- 2023-11-23\_20-25-01\_rev-2006-01-01T00:00:00Z.csv
- 2023-11-23\_20-25-01\_rev-2008-01-01T00:00:00Z.csv
- 2023-11-23\_20-25-01\_rev-2010-01-01T00:00:00Z.csv
- 2023-11-23\_20-25-01\_rev-2012-01-01T00:00:00Z.csv
- 2023-11-23\_20-25-01\_rev-2014-01-01T00:00:00Z.csv
- 2023-11-23\_20-25-01\_rev-2016-01-01T00:00:00Z.csv
- 2023-11-23\_20-25-01\_rev-2018-01-01T00:00:00Z.csv
- 2023-11-23\_20-25-01\_rev-2020-01-01T00:00:00Z.csv
- 2023-11-23\_20-25-01\_rev-2022-01-01T00:00:00Z.csv

The script's configuration is saved in 2023-11-23\_20-25-01\_settings.txt

## Using Gephi to draw the graphs

I then loaded all the CSV files into Gephi (version 0.10.0), following the [same method as ever](https://framagit.org/retrodev/historic-graphs/-/tree/master/examples/tutorial). I spatialized the nodes using the Fruchterman Reingold algorithm and configured the renderer in order to keep the labels well readble. Using the timeline, I was able to render the state of the graph for each timestep, producing the following images :

- crispr\_link\_graph_01-01-2004.png
- crispr\_link\_graph_01-01-2006.png
- crispr\_link\_graph_01-01-2008.png
- crispr\_link\_graph_01-01-2010.png
- crispr\_link\_graph_01-01-2012.png
- crispr\_link\_graph_01-01-2014.png
- crispr\_link\_graph_01-01-2016.png
- crispr\_link\_graph_01-01-2018.png
- crispr\_link\_graph_01-01-2020.png
- crispr\_link\_graph_01-01-2022.png

One can see how the network of links started to build up starting from 2012, with the CRISPR article in a key position. However, this presentation of the data is somehow fallacious, since all the articles appear on the graph even when they were not yet created. This could lead one to interpret this case as a group of unconnected articles that were progressively linked together while, in fact, half of these article were only created after 2012. This important drawback comes from the way Gephi handles the data produced by Historic Graphs. I have a few ideas of ways to solve it (ideally, giving each node a creation date, and perhaps -- for some other cases -- also a disparition one) but most of these would require more time than I currently have.



