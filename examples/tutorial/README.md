# Tutorial

A quick demo showing how to capture the link structure around an article at different points in time, and how to visualize it as a dynamic map using Gephi.

## What it is all about

Historic Graphs is mainly a python script, historic-graphs.py, using Wikipedia APIs to collect data from articles' version history. Starting from an article, the script retrieves a list of the links it contained at different points in time. It will then follow these links and continue collecting lists of links for the same dates, up to the desired depth. All the lists of links are then merged into nice tables, showing the link structure for each date. These tables can then be imported into network analysis tools such as Gephi, to be visualized as dynamic maps.

The script requires Python 3. It has only been tested on a GNU/Linux (Debian) system, but should work properly on others. It relies on the requests library, that can be install through pip : python3 -m pip install requests


## Setting-up data collection

Data collection requires at least four pieces of information :
- the title of the articles from which the collection will start (it is simpler to start with only one article)
- the dates for which links will be collected (usually from 3 to 10 dates, distant from a few hours to a few months)
- the language code of the wiki that will be used (en, fr ... these two are the only one properly tested yet)
- the depth of the map (collection time increases exponentially, so start with only 1)

This information needs to be inserted into the python script. Open the script with your favorite text editor and change the configuration variables accordingly. For example, if I want to study the long term evolution of the Dorayaki article on the english wiki :

	pages = ['Dorayaki']
	dates = ['2006-01-01T00:00:00Z', '2012-01-01T00:00:00Z', '2018-01-01T00:00:00Z']
	lang = 'en'
	depth = 1

The other configuration variables refer to more advanced options that will not be covered in this tutorial.


## Starting to collect

To start collecting data, open a terminal and run the python script. For example, on a GNU/Linux system :

	python3 historic-graphs.py

The collection process is slow (around 8 minutes for this small example) because it involves a lot of communications with the API. Have a cup of tea ... and there we are ! The script generates one CSV file per date :

	2019-08-23_15-33-37_rev-2006-01-01T00:00:00Z.csv
	2019-08-23_15-33-37_rev-2012-01-01T00:00:00Z.csv
	2019-08-23_15-33-37_rev-2018-01-01T00:00:00Z.csv

Each file contains the list of the links that existed at one of these dates (see the second part of the file name -- the first part is the data collection date). Every line in a file (except the header line) represents a link, with the source and target articles, and a timestamp :

	source	target	timeset
	Red bean paste	Wagashi	2006-01-01T00:00:00Z
	Red bean paste	Dorayaki	2006-01-01T00:00:00Z
	Oolong (rabbit)	Internet meme	2006-01-01T00:00:00Z
	Samurai	Japan	2006-01-01T00:00:00Z
	Samurai	Anime	2006-01-01T00:00:00Z
	Samurai	Manga	2006-01-01T00:00:00Z
	Ueno	Tokyo	2006-01-01T00:00:00Z
	...

The script also generates a setting file, to remind you what configuration variables were used to generate this dataset.


## Mapping with Gephi

_The screenshots and terms used in this section refer to the version 0.9.2 of Gephi. There might be some differences with other versions._

To map this dataset, firstly create a new Gephi project. Then, use the "open" menu to import all three files at the same time (you can select several files with ctrl+click or shift+click)

![Gephi's files selection window](img/gephi_selecting_files.png)

For each file, two importation windows will be displayed. On the first one, check that "Separator" is set to "Tabulation", and "As table" to "Edge table". On the second, check that the "Time representation" is set to "Timestamps" and that the "timeset" column is detected as "TimestampSet".

![Gephi's importation window 1](img/gephi_importation_window1.png)
![Gephi's importation window 2](img/gephi_importation_window2.png)

After all the files have been processed, a final window appears, showing the numbers of files/nodes/edges that have been imported. On this screen, you need to change the radio-button on the bottom-right, so that all three files are merged in the same space (and not imported in three different spaces).

![Gephi's importation window 3](img/gephi_importation_window3.png)

You now have you data imported. You can play with spatialisation and all.

![Mapping with Gephi](img/gephi_mapping1.png)

To use the "dynamic" systems, you need to activate the timeline on the bottom panel. You can change the size of the display segment and move it left or right. The edges that were not active during the selected timeframe will disappear from the graph.

![Using Gephi's timeline](img/gephi_mapping2.png)

For some reasons, displaying the nodes' labels requires a small tweak. You need to switch to the "Data laboratory" view and copy the data from the "Id" column to the "Label" one. Yes, this is not very convenient ...


## Do it yourself

You can reproduce this demo using the python script with the same configuration variables. I also uploaded the resulting CSV files, if you want to skip the data collection step. I also provided the final graph file (2019-08-23_15-33-37.gephi), if you only want to try the timeline and spatialisation tools. Have fun !


## Credit

Historic Graphs was created and is maintained by Antonin Segault (Paris Nanterre University, Dicen-IdF lab, France). For questions, suggestions and bug fixes : antonin.segault-AT-parisnanterre.fr

Historic Graphs is a free software. The python script, the documentation and the examples are released under the terms of the GNU General Public License version 3 : https://www.gnu.org/licenses/

